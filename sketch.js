
const step = 25;
const model = tf.sequential();
var erreurLabel;


const listeX = [
	[0, 0],
	[0, 1],
	[1, 0],
	[1, 1],
];

const listeY = [
	[0],
	[1],
	[1],
	[0]
];

async function train() {
	const xTrain = tf.tensor2d(listeX);
	const yTrain = tf.tensor2d(listeY);
	const response = await model.fit(xTrain, yTrain, {
		epochs: 1,
		shuffle: true
	});
	xTrain.dispose();
	yTrain.dispose();
	return response.history;
}

function setup() {
	createCanvas(500, 500);
	erreurLabel = document.querySelector(".erreur");

	const layer1 = tf.layers.dense({
		units: 5,
		activation: 'sigmoid',
		inputShape: [2]
	});

	const layer2 = tf.layers.dense({
		units: 1,
		activation: 'sigmoid'
	});
	model.add(layer1);
	model.add(layer2);

	model.compile({
		optimizer: tf.train.adam(0.1),
		loss: tf.losses.meanSquaredError
	});

	session();
}

function draw() {
	console.log(tf.memory().numTensors);
}

async function session() {
	train().then((history) => {

		erreurLabel.innerHTML = "Erreur : " + history.loss[0] + "%";
		background(255);
		//get fata
		const inputs = [];

		for (let x = 0; x < width; x += step) {
			for (let y = 0; y < height; y += step) {

				const xI = map(x, 0, width, 0, 1);
				const yI = map(y, 0, height, 0, 1);
				inputs.push([xI, yI]);
			}
		}

		const input = tf.tensor2d(inputs);
		const output = model.predict(input);
		const data = output.dataSync();
		for (let x = 0; x < width; x += step) {
			for (let y = 0; y < height; y += step) {
				const index = (y / step) * (width / step) + (x / step);
				const color = (map(data[index], 0, 1, 0, 255));
				fill(color);
				rect(x, y, step, step);
				fill(255)
				textSize(8);
				text(Number.parseFloat(data[index]).toPrecision(3),x+step/3,y+step/2);

			}
		}

		input.dispose();
		output.dispose();
		session();
	});
}